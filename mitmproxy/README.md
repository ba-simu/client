# README

> [!WARNING]
> The following guide is based on Windows OS.

## Prerequisite

- In your device
  - [ブルアカ](https://play.google.com/store/apps/details?id=com.YostarJP.BlueArchive)
  - [Wireguard](https://download.wireguard.com/android-client/)
- In your PC
  - [mitmproxy](https://mitmproxy.org/)

## Init

1. Launch mitmproxy. It will show an QR code.

   ```bash
   mitmweb -m wireguard
   ```

1. Open Wireguard, add a tunnel from the QR code, and turn it on.
1. Install mitmproxy certificate on the device.

   - iOS

      Browse [mitm.it](mitm.it) and follow the guide.

   - LDPlayer 9

      ```bash
      # cd to LDPlayer9
      .\adb.exe start-server
      .\adb.exe devices # make sure LDPlayer is auto connected
      .\adb.exe root
      .\adb.exe remount
      .\adb.exe push "~\.mitmproxy\mitmproxy-ca-cert.pem" "/etc/security/cacerts/"
      # Use this instead if you are using cmd but not PowerShell.
      # .\adb.exe push "%userprofile%\.mitmproxy\mitmproxy-ca-cert.pem" "/etc/security/cacerts/"
      .\adb.exe shell "mv /etc/security/cacerts/mitmproxy-ca-cert.pem /etc/security/cacerts/c8750f0d.0"
      .\adb.exe shell "chmod 644 /etc/security/cacerts/c8750f0d.0"
      ```

   - MuMuPlayer ([official guide](https://mumu.163.com/help/20221018/35047_1047210.html))

      ```bash
      # cd to MuMuPlayerGlobal-12.0/shell
      .\adb.exe connect localhost:7555
      .\adb.exe root # need to confirm in the emulator
      .\adb.exe shell "mount -o remount,rw /system"
      .\adb.exe push "~\.mitmproxy\mitmproxy-ca-cert.pem" "/etc/security/cacerts/"
      # Use this instead if you are using cmd but not PowerShell.
      # .\adb.exe push "%userprofile%\.mitmproxy\mitmproxy-ca-cert.pem" "/etc/security/cacerts/"
      .\adb.exe shell "mv /etc/security/cacerts/mitmproxy-ca-cert.pem /etc/security/cacerts/c8750f0d.0"
      .\adb.exe shell "chmod 644 /etc/security/cacerts/c8750f0d.0"
      ```

1. Write a valid server in [server.txt](./server.txt)

1. Done!

## Usage

> [!WARNING]
> Close mitmproxy if you just inited.

1. Double click [start.bat](./start.bat)
1. Open Wireguard.
1. Open ブルアカ.
1. Enjoy!
