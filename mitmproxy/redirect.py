import json
from pathlib import Path

from mitmproxy import http


root = Path(__file__).parent
with open(root / "server.txt", encoding="utf8") as f:
    address = f.read()
    arr = address.split(":")
    if len(arr) != 2:
        raise Exception("Invalid server.")
    SERVER_HOST = arr[0]
    SERVER_PORT = int(arr[1])


SERVER_URL = address
print(f"Redirect server to {SERVER_URL}\n")


def request(flow: http.HTTPFlow) -> None:
    if (
        flow.request.pretty_host.endswith("log.aliyuncs.com")
        or flow.request.pretty_host == "prod-logcollector.bluearchiveyostar.com"
    ):
        flow.kill()
        return
    if flow.request.pretty_host == "ba-jp-sdk.bluearchive.jp":
        flow.request.scheme = "http"
        flow.request.host = SERVER_HOST
        flow.request.port = SERVER_PORT
        return


def response(flow: http.HTTPFlow) -> None:
    if flow.request.pretty_host == "yostar-serverinfo.bluearchiveyostar.com":
        res = flow.response
        if res is None:
            return
        text = res.text
        if text is None:
            return
        data = json.loads(text)
        cg = data["ConnectionGroups"][0]
        cg["ApiUrl"] = f"http://{SERVER_URL}/api/"
        # "CustomerServiceUrl": "https://bluearchive.jp/contact-1-hint",
        cg["GatewayUrl"] = f"http://{SERVER_URL}/api/"
        cg["KibanaLogUrl"] = f"http://{SERVER_URL}/log"
        # "ManagementDataUrl": "https://prod-noticeindex.bluearchiveyostar.com/prod/index.json",
        cg["ProhibitedWordBlackListUri"] = cg["ProhibitedWordWhiteListUri"]
        ocg = cg["OverrideConnectionGroups"][-1]
        ver = ocg["AddressablesCatalogUrlRoot"].split("/")[-1]
        ocg["AddressablesCatalogUrlRoot"] = f"http://{SERVER_URL}/data/{ver}"
        res.text = json.dumps(data)
